$(document).ready(function() {

	date = "03/01/2015 12:00";
    Timer(date);
    setInterval(function() {
        Timer(date);
    }, 1000);

	$('.menu-top a, .scroll-link').click(function() {
		var el = $(this).data('el');
		$('html, body').animate({
	        scrollTop: $('#'+el).offset().top - 90
	    }, 500);
	    return false;
	});

	$('input[name=phone]').mask('+375 (99) 999-99-99');
	$('.bailo-single').click(function() {
		if (!$(this).hasClass('bailo-active')) {
			var color = $(this).data('color');
			$('.bailo-single').removeClass('bailo-active');
			$('.hands-list img').removeClass('visible-hand');
			$('.hands-list img[data-color=' + color +']').addClass('visible-hand');
			$(this).addClass('bailo-active');
			$('.bailo-single-info-title').fadeOut(500);
			$('.bailo-single-info-title[data-color=' + color +']').fadeIn(500);

		}
		return false;
	});

	$('.previews-list a').magnificPopup({
		type: 'image',
		mainClass: 'mfp-fade',
		removalDelay: 500,
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}
	});

	if ($(window).width() > 1453) {
		var offset = ($(window).width()-960)/2;
		$('.hands-list img').css('left', '-' + offset + 'px');
	}
	$(window).resize(function() {
		if ($(window).width() > 1453) {
			var offset = ($(window).width()-960)/2;
			$('.hands-list img').css('left', '-' + offset + 'px');
		} else {
			$('.hands-list img').removeAttr('style');
		}
	});

	$('.advantage-single').on('scrollSpy:enter', function() {
	    $('.img-wrap').addClass('visible');
	    setTimeout(function() {
	    	$('.advantage-title, .advantage-desc').addClass('visible');
	    }, 450);
	});
	$('.advantage-single').on('scrollSpy:exit', function() {
	    $('.img-wrap').removeClass('visible');
	    $('.advantage-title, .advantage-desc').removeClass('visible');
	});

    $('.step').eq(0).on('scrollSpy:enter', function() {
    	var speed = 250;
        $(".step-1").stop().animate({'opacity': 1}, speed);
        $(".step-arrow").eq(0).stop().delay(speed).animate({'opacity': 1}, speed);
        $(".step-2").stop().delay(speed*2).animate({'opacity': 1}, speed);
        $(".step-arrow").eq(1).stop().delay(speed*3).animate({'opacity': 1}, speed);
        $(".step-3").stop().delay(speed*4).animate({'opacity': 1}, speed);
        $(".step-arrow").eq(2).stop().delay(speed*5).animate({'opacity': 1}, speed);
        $(".step-4").stop().delay(speed*6).animate({'opacity': 1}, speed);
    });
    $('.step').eq(0).on('scrollSpy:exit', function() {
    	$(".step, .step-arrow").animate({'opacity': 0}, 100);
    });

    $('footer').on('scrollSpy:enter', function() {
    	$('.footer-title-wrapper h3').css('font-size', '54px');
    	setTimeout(function() {
    		$('.bailo-blisters').css({'left': '0px', 'opacity': '1'});
    	}, 300);
    });
    $('.footer-title-wrapper').on('scrollSpy:exit', function() {
    	$('.footer-title-wrapper h3').css('font-size', '10px');
    	$('.bailo-blisters').css({'left': '-150px', 'opacity': '0'});
    });

	$('.advantage-single').scrollSpy();
	$('.step').scrollSpy();
	$('footer').scrollSpy();
	$('.footer-title-wrapper').scrollSpy();
});

function Timer(string) {
    var date_new = string;
    var date_t = new Date(date_new);
    var date = new Date();
    var timer = date_t - date;
    if (date_t > date) {
        var day = parseInt(timer / (60 * 60 * 1000 * 24));
        if (day < 10) {
            day = "0" + day;
        }
        day = day.toString();
        var hour = parseInt(timer / (60 * 60 * 1000)) % 24;
        if (hour < 10) {
            hour = "0" + hour;
        }
        hour = hour.toString();
        var min = parseInt(timer / (1000 * 60)) % 60;
        if (min < 10) {
            min = "0" + min;
        }
        min = min.toString();
        var sec = parseInt(timer / 1000) % 60;
        if (sec < 10) {
           	sec = "0" + sec;
        }
        sec = sec.toString();
        timethis = day + " : " + hour + " : " + min + " : " + sec;
        $(".days.timer-text").text(day);
        $(".hours.timer-text").text(hour);
        $(".minutes.timer-text").text(min);
        $(".seconds.timer-text").text(sec);
    } else {
        $(".days.timer-text").text("00");
        $(".hours.timer-text").text("00");
        $(".minutes.timer-text").text("00");
        $(".seconds.timer-text").text("00");
    }
}